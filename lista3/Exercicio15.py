# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    15 - Faça um algoritmo que seja capaz de obter um quociente inteiro da divisão de dois números
    fornecidos, sem utilizar a operação de divisão (/) e nem divisão inteira (div).
'''

dividendo = float(input("Informe o valor do dividendo: "))
divisor = float(input("Informe o valor do divisor: "))

aux = 0
i = 0

resto = 0
if divisor == 0:
    print("Na computação não é possível dividir um número por 0. "
          "Caso o divisor seja 0, deverá lançar uma exceção(Ex.: Arithmetic Exception), qual deverá ser tratada.")
else:
    while aux <= dividendo:
        aux = aux + divisor
        i += 1
        resto = dividendo % divisor

    print("Quociente da divisão é: ", i - 1)




