# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    34 - Um cinema possui capacidade de 100 lugares e está sempre com ocupação total. Certo dia,
    cada espectador respondeu a um questionário, no qual constava:
    
    * sua idade;
    * sua opinião em relação ao filme, segundo as seguintes notas:
    
    Nota        Significado
    A           Ótimo
    B           Bom
    C           Regular
    D           Ruim
    E           Péssimo
    
    Elabore um algoritmo que, lendo esses dados calcule e imprima:
    
    * a quantidade de respostas Ótimo;
    * a diferença porcentual entre respostas Bom e Regular;
    * a média de idade das pessoas que responderam Ruim;
    * a porcentagem de respostas Péssimo e a maior idade que utilizou essa opção;
    * a diferença de idade entre a maior idade que respondeu Ótimo e a maior idade que respondeu Ruim.
'''
