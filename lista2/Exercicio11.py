# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    11 - Construa um algoritmo que seja capaz de dar a classificação olímpica de 3 países informados. Para cada país é
    informado o nome, a quantidade de medalhas de ouro, prata e bronze. Considere que cada medalha de ouro tem peso 3,
     cada prata tem peso 2 e cada bronze 1.
'''

pais1 = "Brasil"
pais2 = "Estados Unidos"
pais3 = "China"

br_gold = 1 # 3
br_silver = 1  # 4
br_bronze = 3  # 3

eu_gold = 0  # 3
eu_silver = 1  # 2
eu_bronze = 4  # 4

chi_gold = 0  # 3
chi_silver = 1  # 2
chi_bronze = 4  # 3

br_total = br_gold * 3 + br_silver * 2 + br_bronze * 1
eu_total = eu_gold * 3 + eu_silver * 2 + eu_bronze * 1
ch_total = chi_gold * 3 + chi_silver * 2 + chi_bronze * 1

print("Colocação\tPaís\t\tTotal\tGold\tSilver\tBronze")
if br_total > eu_total and br_total > ch_total:
    print("1°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

    if eu_total > ch_total:
        print("2° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
        print("3°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
    else:
        print("2°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
        print("3° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))

elif eu_total > br_total and eu_total > ch_total:
    print("1° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))

    if br_total > ch_total:
        print("2°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))
        print("3°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
    else:
        print("2°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
        print("3°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

elif ch_total > br_total and ch_total > eu_total:
    print("1°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))

    if br_total > eu_total:
        print("2°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))
        print("3° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
    else:
        print("2° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
        print("3°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

elif br_total == eu_total == ch_total:

    if br_gold > eu_gold and br_gold > chi_gold:
        print("1°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

        if eu_gold > chi_gold:
            print("2° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
            print("3°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
        else:
            print("2°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
            print("3° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))

    elif eu_gold > br_gold and eu_gold > chi_gold:
        print("1° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))

        if br_gold > chi_gold:
            print("2°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))
            print("3°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
        else:
            print("2°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
            print("3°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

    elif chi_gold > br_gold and chi_gold > eu_gold:
        print("1°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))

        if br_gold > eu_gold:
            print("2°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))
            print("3°", pais2)
        else:
            print("2° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
            print("3°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

    if br_gold == eu_gold and br_gold == chi_gold:
        if br_silver > eu_silver and br_silver > chi_silver:
            print("1°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

            if eu_silver > chi_silver:
                print("2° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
                print("3°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
            else:
                print("2°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
                print("3° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))

        elif eu_silver > br_silver and eu_silver > chi_silver:
            print("1° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))

            if br_silver > chi_silver:
                print("2°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))
                print("3°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
            else:
                print("2°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
                print("3°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

        elif chi_silver > br_silver and chi_silver > eu_silver:
            print("1°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))

            if br_silver > eu_silver:
                print("2°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))
                print("3° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
            else:
                print("2° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
                print("3°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

        elif br_silver == eu_silver and br_silver == chi_silver:
            if br_bronze > eu_bronze and br_bronze > chi_bronze:
                print("1°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

                if eu_bronze > chi_bronze:
                    print("2° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
                    print("3°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
                else:
                    print("2°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
                    print("3° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))

            elif eu_bronze > br_bronze and eu_bronze > chi_bronze:
                print("1° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))

                if br_bronze > chi_bronze:
                    print("2°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))
                    print("3°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
                else:
                    print("2°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))
                    print("3°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))

            elif br_bronze == eu_bronze and br_bronze == chi_bronze:
                print("1°\t\t   ", pais1 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (br_total, br_gold, br_silver, br_bronze))
                print("2° ", pais2 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (eu_total, eu_gold, eu_silver, eu_bronze))
                print("3°\t\t   ", pais3 + "\t\t%d\t\t%d\t\t%d\t\t%d" % (ch_total, chi_gold, chi_silver, chi_bronze))

