# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    3. Supondo  que  a  população  de  um  país  A  seja  da  ordem  de  80000  habitantes  com  uma  taxa anual de
crescimento de 3% e que a população de B seja 200000 habitantes com uma taxa de crescimento de   1.5%.   Faça   um
programa   que   calcule  e   escreva   o   número   de   anos necessários para  que  a  população  do  país  A
ultrapasse  ou  iguale  a  população  do  país  B, mantidas astaxas de crescimento
'''

anos = 0
pop_pais_a = 80000
pop_pais_b = 200000

while pop_pais_a < pop_pais_b:
    pop_pais_a = (pop_pais_a * 1.03)
    pop_pais_b = (pop_pais_b * 1.015)
    anos += 1

print("São necessários %d anos para que a população do país A ultrapasse ou iguale a população do país B." % anos)