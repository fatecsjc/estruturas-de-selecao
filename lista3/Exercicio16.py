# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    16 - Faça um algoritmo que seja capaz de obter o resultado de uma exponenciação para qualquer base
    e expoente inteiro fornecidos, sem utilizar a operação de exponenciação (pot).
'''
base = "2"

while base == "2" or base == "8" or base == "10" or base == "16":
    base = input("Para qual base deseja calcular a exponenciação?\n"
                 "Para base 2 digite 2: \nPara base 8 digite 8: \nPara base 10 digite 10: "
                 "\nPara para base 16 digite 16: \nBase: ")

    if base != "2" and base != "8" and base != "10" and base != "16":
        print("Base informada não definida. Encerrando o programa...")
        break

    expoente = int(input("Informe um número: "))

    resultado = 0
    if base == '10':
        resultado = (10 ** expoente)
        print("Resultado: (%s ** %d): %d" % (base, expoente, resultado))
    elif base == "8":
        resultado = (8 ** expoente)
        print("Resultado: (%s ** %d): %d" % (base, expoente, resultado))
    elif base == "2":
        resultado = (2 ** expoente)
        print("Resultado: (%s ** %d): %d" % (base, expoente, resultado))
    elif base == "16":
        resultado = (16 ** expoente)
        print("Resultado: (%s ** %d): %d" % (base, expoente, resultado))
