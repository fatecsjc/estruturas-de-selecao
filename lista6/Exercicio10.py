# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    10. Faça um programa que peça um inteiro positivo e o mostre invertido. Ex.: 1234 gera 4321
'''

numero = input("Informe um número: ")

invertido = numero[::-1]
print(int(invertido))
