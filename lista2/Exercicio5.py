# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    5 - Dada uma determinada data de aniversário (dia, mês e ano separadamente), elabore um algoritmo que solicite
    a data atual (dia, mês e ano separadamente) e calcule a idade em ano, em meses e em dias.
'''
dia_niver = 26
mes_niver = 8
ano_niver = 1979

dia_atual = 26
mes_atual = 8
ano_atual = 2019

anos = ano_atual - ano_niver
meses = anos * 12
dias = meses * 30

print("Anos: ", anos)
print("Meses: ", meses)
print("Dias: ", dias)
