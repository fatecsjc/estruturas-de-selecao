# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    32 - Anacleto tem 1,50 metro e cresce 2 centímetros por ano, enquanto Felisberto tem 1,10 metro
    e cresce 3 centímetros por ano. Construa um algoritmo que calcule e imprima quantos anos serão
    necessários para que Felisberto seja maior que Anacleto.
'''
