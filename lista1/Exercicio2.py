# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    Exercícios Práticos - Algoritmos Sequenciais
    
    2 - Dada uma determinada data de aniversário (dia, mês e ano separadamente), elabore um algoritmo que solicite
    a data atual (dia, mês e ano separadamente) e calcule a idade em anos, em meses e em dias - Assuma que 1 ano
    tem 360 dias e 1 mês 30 dias.
'''

dia_niver = 26
mes_niver = 8
ano_niver = 1979

dia_atual = 26
mes_atual = 8
ano_atual = 2019

anos = ano_atual - ano_niver
meses = anos * 12
dias = meses * 30

print("Anos: ", anos)
print("Meses: ", meses)
print("Dias: ", dias)
