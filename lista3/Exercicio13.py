# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    13 - Elabore um algoritmo que obtenha o mínimo múltiplo comum (MMC) entre
    dois números fornecidos.
'''

print("Para calcular o MMC, informe o 1° e o 2° número")
num1 = int(input("Informe o 1° número: "))
num2 = int(input("Informe o 2° número: "))

x, y, resto = num1, num2, None

while resto != 0:
    resto = x % y
    x = y
    y = resto

print("MMC entre %d e %d é: %d " % (num1, num2, (num1 * num2 / x)))
