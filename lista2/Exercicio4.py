# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    4 - Ao completar o tanque de combustível de um automóvel, faça um algoritmo que calcule o consumo efetuado, assim
    como a autonomia que o carro ainda teria antes do abastecimento. Considere que o veículo sempre seja abastecido
    até encher o tanque e que são fornecidas apenas a capacidade do tanque, a quantidade de litros abastecidos e a
    quilometragem percorrida desde o último abastecimento.
'''

# Legenda:
# ca = capacidade_tanque
# qa = quantidade_abastecida
# qp = quilometragem_percorrida
# ce = consumo_efetuado
# a = autonomia

ct = 100  # em litros
qa = 25  # em litros
qp = 100  # Km
ce = (qp / qa)
a = (ct - qa) * ce
print("Consumo por de %d litros por KM.\nAutonomia de %d Km." % (ce, a))



