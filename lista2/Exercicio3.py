# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    3 - Prepare um algorítmo capaz de inverter um número de 3 dígitos fornecido, ou seja, apresentar primeiro a unidade,
    depois a dezena e a centena.
'''

numero = 101
unidade = numero // 1 % 10
dezena = numero // 10 % 10
centena = numero // 100 % 10
print(unidade, dezena, centena)
# print(numero // 1 % 10, numero // 10 % 10, numero // 100 % 10)

# ou
'''
    10. Faça um programa que peça um inteiro positivo e o mostre invertido. Ex.: 1234 gera 4321
'''

numero = input("Informe um número: ")

invertido = numero[::-1]
print(int(invertido))
