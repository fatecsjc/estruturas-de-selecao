# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Opção alternativa: utilização da função pi da biblíoteca Math
# from math import pi

__author__ = 'Robson'

'''
    2 - Elabore um algoritmo que calcule a área de um círculo qualquer de raio fornecido.
'''

pi = 3.14159265359
raio = 10
area = pi * (raio ** 2)
print("Área: %2.2f" % area)

