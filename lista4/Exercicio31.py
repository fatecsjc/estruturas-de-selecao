# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    31 - Foi realizada uma pesquisa sobre algumas características físicas da população de uma certa região,
    a qual coletou os seguintes dados referentes a cada habitante para análise:
    
    * sexo('M' - masculino ou 'F' - feminino);
    * cor dos olhos('A' - azuis, 'V' - verdes ou 'C' - castanhos);
    * cor dos cabelos('L' - loiros, 'C' - castanhos ou 'P' - pretos);
    * idade.
    
    Faça um algoritmo que determine e escreva:
    
    * maior idade dos habitantes;
    * porcentagem entre os indivíduos do sexo masculino, cuja idade está entre 18 e 35 anos, inclusive;
    * a porcentagem do total de indivíduos do sexo feminino cuja idade está entre 18 e 35 anos, inclusive e
    que tenham olhos verdes e cabelos loiros.
    
    O final do conjunto de habitantes é reconhecido pelo valor -1 entrando como idade.
'''
