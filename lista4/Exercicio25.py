# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    25 - Prepare um algoritmo que calcule o valor de H, sendo que ele é determinado pela série
    H = 1/1 + 3/2 + 5/3 + 7/4 + ... + 99/50.
'''

dividendo = 1
divisor = 1

x = 0
while x < 50:
    h = str(dividendo) + "/" + str(divisor)
    print(h)
    dividendo = dividendo + 2
    divisor += 1
    x += 1
