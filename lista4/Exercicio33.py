# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    33 - Realizou-se uma pesquisa para determinar alguns dados estatísticos em relação ao conjunto de crianças
     nascidas em um certo período de uma determinada maternidade. Construa um algoritmo que leia o número de 
     crianças nesse período e, depois, em um número indeterminado de vezes, o sexo de um recém-nascido prematuro
     ('M' - masculino ou 'F' - feminino) e o número de dias que este foi mantido na incubadora.
     
     Como finalizador, teremos a letra 'X' no lugar do sexo da criança. Determine e imprima:
     
     * a porcentagem de recém-nascidos prematuros;
     * a porcentagem de recém-nascidos meninos e meninas do total de prematuros;
     * a média de dias de permanência dos recém-nascidos prematuros na incubadora;
     * o maior número de dias que um recém-nascido prematuro permaneceu na incubadora.
'''
