# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    Exercícios Práticos - Algoritmos Sequenciais
    
    3 - Um dado comerciante maluco cobra 10% de acréscimo para cada prestação em atraso e depois dá um
    desconto  de 10% sobre esse valor.
    Faça um algoritmo que solicite o valor da prestação em atraso e apresente o valor final a pagar, assim
    como o prejuízo do comerciante na operação em relação ao valor da prestação original.
'''

valor = float(input("Informe o valor da prestação: "))
print("Valor final a pagar: %2.2f" % (valor * 1.1 * 0.9))
print("Prejuízo: %2.2f" % (valor - (valor * 1.1) * 0.9))
