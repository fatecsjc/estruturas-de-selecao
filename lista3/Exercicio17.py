# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    17 - Construa um algoritmo que gere os 20 primeiros termos de uma série tal qual a de Fibonacci, mas
    que cujos 2 primeiros termos são fornecidos pelo usuário.
'''

print("Informe dois numeros: ")
num1 = int(input("Informe o 1° termo: "))
num2 = int(input("Informe o 2° termo: "))

i = 0

while i != 20:
    aux = num2
    fibo = num1 + num2
    num2 = num1
    num1 = fibo

    i += 1

    print(i, num2, aux, fibo)
