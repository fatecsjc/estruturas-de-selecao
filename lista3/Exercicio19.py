# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    19 - A  conversão de graus Fahrenheit para centígrados é obtida pela fórmula C = 5/9 (F - 32).
    Escreva um algoritmo que calcule e escreva uma tabela de graus centígrados em função de graus
    Fahrenheit que variem de 50 a 150 de 1 em 1.
'''

# Obs.: no livro está 5/9, mas creio que o correto é 9 / 5 ou seja 1,8

# range(inicio, fim, intervalo)
print("Celsius |\tFahreinheit")
for c in range(50, 151, 1):
    f = (c * (9 / 5)) + 32
    print("%2.2f \t\t%2.2f" % (c, f))
