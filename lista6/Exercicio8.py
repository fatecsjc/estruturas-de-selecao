# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    8. Verifique se um inteiro positivo N é primo.
'''
contador = 1
divisor = 0

numero = int(input("Informe um número: "))

while contador <= numero:
    if numero % contador == 0:
        divisor += 1
    contador += 1

if divisor == 2:
    print("%d é um número primo." % numero)
else:
    print("%d não é um número primo." % numero)