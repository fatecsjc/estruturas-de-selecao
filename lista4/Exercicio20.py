# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    20 - Uma rainha requisitou os serviços de um monge e disse-lhe que pagaria qualquer preço. O monge, 
    necessitando de alimentos, perguntou à rainha se o pagamento poderia ser feito com grãos de trigo 
    dispostos em um tabuleiro de xadrez, de tal forma que o primeiro quadro contivesse apenas um grão 
    e os quadros subsequentes, o dobro do quadro anterior. A rainha considerou o pagamento barato e 
    pediu que o serviço fosse executado, sem se dar conta de que seria impossível efetuar o pagamento. 
    Faça um algoritmo para calcular o número de grãos que o monge esperava receber.
'''
'''
    [1, 2, 3], 
    [4, 5, 6], 
    [7, 8, 9]
'''

