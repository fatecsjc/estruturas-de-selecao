# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    35 - Em um prédio há três elevadores denominados A, B e C. Para otimizar o sistema de 
    controle de elevadores foi realizado um levantamento no qual cada usuário respondia:
    
    * o elevador que utilizava com mais frequência;
    * o período em que utilizava o elevador, entre: 'M' - matutino, 'V' - vespertino e 'N' - noturno.
    
    Construa um algoritmo que calcule e imprima:
    
    * qual é o elevador mais frequentado e em que período se concentra o maior fluxo;
    * qual o período mais usado de todos e a que elevador pertence;
    * qual a diferença porcentual entreo mais usado dos horários e o menos usado;
    * qual a porcentagem sobre o total de serviços prestados do elevador de média utilização.
'''
