# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    18 - Construa um algoritmo que, dado um conjunto de valores inteiros e positivos, determine qual
    o menor e o maior valor do conjunto. O final do conjunto de valores é conhecido pelo valor - 1,
    que não deve ser considerado.
'''
conjunto = [71, 14, 77, 22, 62, 81, 49, 80, 88]
menor, maior = conjunto[0], conjunto[0]

for i in conjunto:
    if i > maior:
        maior = i
    elif i < menor:
        menor = i

print("Conjunto: ", conjunto)
print("Menor: %d\tMaior: %d" % (menor, maior))
