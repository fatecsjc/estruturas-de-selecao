# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    28 - Construa um algoritmo que calcule o valor dos dez primeiros termos da série H, em que:
    H = 1/pot(1, 3) - 1/pot(3, 3) + 1/pot(5, 3) - 1/pot(7, 3) + 1/pot(9, 3) - ...
'''

base = 1
expoente = 3
dividendo = 1
x = 0

while x < 10:
    h = str(base) + "/" + str(expoente)
    print(h)
    base = base + 2
    base = pow(base, expoente)
    expoente = expoente
    x += 1

