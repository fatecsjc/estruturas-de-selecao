# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    Exercícios Práticos - Algoritmos Sequenciais
    
    4 - Elabore um algoritmo capaz de inverter um número de 3 dígitos, fornecido, ou seja, apresentar primeiro
    a unidade e depois a dezena e centena.
'''

numero = 119
unidade = numero // 1 % 10
dezena = numero // 10 % 10
centena = numero // 100 % 10
print(unidade, dezena, centena)
# print(numero // 1 % 10, numero // 10 % 10, numero // 100 % 10)