# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)

__author__ = 'Robson'

'''
    21 - Em uma eleição presidencial existem quatro candidatos. OS votos são informados por código. Os dados
    utilizados para a escrutinagem obedecem à seguinte codificação:
    * 1, 2, 3, 4 = voto para os respectivos candidatos;
    * 5 = voto nulo;
    * 6 = voto em branco.
    Elabore um algoritmo que calcule e escreva:
    * o total de votos para cada candidato e seu porcentual sobre o total;
    * o total de votos nulos e seu porcentual sobre o total;
    * o total de votos em branco e seu porcentual sobre o total.
    Como finalizador do conjunto de votos, tem-se o valor 0.
'''

voto = -1

votos_candidatos = [0, 0, 0, 0, 0, 0]

while voto != 0:
    print("Eleições 2019")

    for i in range(1, 8):
        if i < 5:
            print(f"Para votar no candidato {i} digite {i}")
        if i == 5:
            print("Para votar nulo, digite %d" % i)
        if i == 6:
            print(f'Para votar em branco, digite {i}')
        elif i == 7:
            print('Para sair digite 0')

    voto = int(input("Escolha seu candidato: "))

    if voto == 1:
        votos_candidatos[0] = votos_candidatos[0] + 1

    if voto == 2:
        votos_candidatos[1] = votos_candidatos[1] + 1

    if voto == 3:
        votos_candidatos[2] = votos_candidatos[2] + 1

    if voto == 4:
        votos_candidatos[3] = votos_candidatos[3] + 1

    if voto == 5:
        votos_candidatos[4] = votos_candidatos[4] + 1

    if voto == 6:
        votos_candidatos[5] = votos_candidatos[5] + 1

    elif voto == 0:
        print("Eleições encerradas.")

total_votos = votos_candidatos[0] + votos_candidatos[1] + votos_candidatos[2] + votos_candidatos[3] \
              + votos_candidatos[4] + votos_candidatos[5]

por = total_votos * 100
print("Total de votos: ", total_votos)
print(f"Votos candidato1: {votos_candidatos[0]}\tPorcentual votos: %2.2f" % (votos_candidatos[0] / total_votos * 100))
print(f"Votos candidato2: {votos_candidatos[1]}\tPorcentual votos: %2.2f" % (votos_candidatos[1] / total_votos * 100))
print(f"Votos candidato3: {votos_candidatos[2]}\tPorcentual votos: %2.2f" % (votos_candidatos[2] / total_votos * 100))
print(f"Votos candidato4: {votos_candidatos[3]}\tPorcentual votos: %2.2f" % (votos_candidatos[3] / total_votos * 100))
print(f"Votos nulo: {votos_candidatos[4]}\tPorcentual votos: %2.2f" % (votos_candidatos[4] / total_votos * 100))
print(f"Votos branco: {votos_candidatos[5]}\tPorcentual votos: %2.2f" % (votos_candidatos[5] / total_votos * 100))
