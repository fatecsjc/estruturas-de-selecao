# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    22 - Escreva um algoritmo que imprima todas as possibilidades de que no lançamento de dois dados tenhamos
    o valor 7 como resultado da soma dos valores de cada dado.
'''

dado1 = [6, 5, 4, 3, 2, 1]
dado2 = [1, 2, 3, 4, 5, 6]

for d1 in dado1:
    for d2 in dado2:
        if d1 + d2 == 7:
            print("%d + %d = 7" % (d1, d2))
