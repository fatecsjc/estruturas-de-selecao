# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    Exercícios Práticos - Algoritmos Sequenciais
    
    1 - Construa um algoritmo que calcula a média ponderada entre 5 números quaisquer (informados pelo usuário),
    sendo que os pesos a serem aplicados são 1, 2, 3, 4 e 5 respectivamente.
'''

n1 = 6.0
n2 = 7.0
n3 = 4.5
n4 = 8.0
n5 = 9.0

media_ponderada = (n1 * 1 + n2 * 2 + n3 * 3 + n4 * 4 + n5 * 5) / 15
print("Média ponderada %2.1f" % media_ponderada)
