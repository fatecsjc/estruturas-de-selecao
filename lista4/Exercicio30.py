# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    30 - Calcule o imposto de renda de um grupo de dez contribuintes, considerando que os dados de
    cada contribuinte, número de CPF, número de dependentes e renda mensal são valores fornecidos pelo 
    usuário. Para cada contribuinte será feito um desconto de 5% do salário mínimo por dependente.
    Os valores da alíquota para cálculo do imposto são:
    
    Renda líquida                       Alíquota
    Até 2 salários mínimos              Isento
    2 a 3 salários mínimos              5%
    3 a 5 salários mínimos              10%
    5 a 7 salários mínimos              15%
    Acima de 7 salários mínimos         20%
    
    Observe que deve ser fornecido o valor atual do salário mínimo 
    para que o algoritmo calcule os valores corretamente.
'''
