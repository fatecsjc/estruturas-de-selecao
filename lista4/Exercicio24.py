# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    24 - Construa um algoritmo que leia um conjunto de dados contendo altura e sexo ('M' para masculino
    e 'F' para feminino) de 50 pessoas e, depois calcule e escreva:
    * a maior e a menor altura do grupo;
    * a média de altura das mulheres;
    * o número de homens e a diferença porcentual entre eles e as mulheres.
'''


