# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    8 - Elabore um algoritmo que, a partir de um dia, mês e ano fornecidos, valide se eles compôem uma data válida. Não
    deixe de considerar os meses com 30 ou 31 dias, e o tratamento do ano bissexto.
'''

dia = int(input("Informe um dia: "))
mes = int(input("Informe um mês: "))
ano = int(input("Informe um ano: "))

bissexto = ano > 1582 and (ano % 400 == 0 or (ano % 4 == 0 and ano % 100 != 0))
data = str(dia) + "/" + str(mes) + "/" + str(ano)

if (mes == 1 or mes == 3 or mes == 5 or mes == 7 or mes == 8 \
        or mes == 10 or mes == 12) and (1 <= dia <= 31):
    print("A data %s é válida." % data)

elif (mes == 4 or mes == 6 or mes == 9 or mes == 11) and (1 <= dia <= 30):
    print("A data %s é válida." % data)

elif mes == 2 and bissexto and 1 <= dia <= 29:
    print("A data %s é válida." % data)

elif 1 <= dia <= 28:
    print("A data %s é válida." % data)
else:
    print("A data %s é inválida." % data)


