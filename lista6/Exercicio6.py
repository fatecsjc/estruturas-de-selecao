# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    6. Dizemos  que  um  número  natural  é triangular se  ele  é  produto  de  três  números  naturais consecutivos.
Exemplo:  120  é  triangular,  pois  4.5.6  =  120.  Dado  um  inteiro  não-negativo n, verificar se N é triangular.
'''

x, y = 1, 0
num = -1

while num < 0:
    num = int(input("Informe um número positivo qualquer: "))
    while y < num:
        y = x * (x + 1) * (x + 2)
        x += 1

if y == num:
    print("%d é um número triangular" % num)
else:
    print("%d não é um número triangular." % num)